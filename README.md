## Informations

Le script principal est main.py.    

Les paramètres sont tous optionnels: ils ont des valeurs par défaut  

Les paramètres disponibles ainsi que leur valeur par défaut sont:
```
key_length=20                   -> Longueur de la clé d'Alice et Bob
message_length=20,              -> Longueur du message initial d'Alice
epochs=1000,                    -> Nombre d'itérations d'apprentissage
learning_rate=0.0008,           -> Taux d'apprentissage de l'Optimizer
batch_size=4096, [Pas encore implémenté]
```
