import tensorflow as tf


class Personnage:
    """
    Représente un personnage du problème, possédant un réseau de neuronnes et mémorisant
    certaines données et fonctions utiles (fonction de loss, messages, clés, etc.)
    """

    def __init__(
            self,
            prenom="Anonyme",
            model=None,
            loss_function=None,
            message_in=None,
            message_out=None,
            cle=None,
            loss=None
    ):
        self.prenom = prenom
        self.cle = cle
        self.message_in = message_in
        self.message_out = message_out
        self.model = model
        self.loss = loss
        self.loss_function = loss_function

    def set_model(self, model):
        self.model = model

    def set_loss_function(self, loss_function):
        self.loss_function = loss_function

    def set_message(self, message):
        if self.cle is not None:
            self.message_in = tf.concat([message, self.cle], axis=1)
        else:
            self.message_in = message

    def set_cle(self, cle):
        self.cle = cle

    def calculate_message_out(self):
        """
        Applique le model du Personnage pour calculer le message de sortie
        :return: liste de 0 et 1 représentant le message de sortie
        """
        self.message_out = self.model(self.message_in)

    def calculate_loss(self, **kwargs):
        """
        Applique la fonction de loss du Personnage
        :param kwargs: Paramètres à appliquer à la fonction de loss
        :return: float: valeur de la loss
        """
        self.loss = self.loss_function(**kwargs)
        return self.loss

    def get_loss_value(self):
        a = self.loss.numpy()
        result = self.loss.numpy()
        return result

    def print_report(self):
        try:
            print(self.prenom + " a une loss de ", self.get_loss_value())
        except:
            print("[ERROR] " + self.prenom + " n'a pas de loss")



