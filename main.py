import copy
import datetime

import tensorflow as tf

from classes import Personnage as P
from utils import args as arg_parser
from utils.tensor_utils_functions import generate_boolean_list, alice_custom_loss_function, bob_custom_loss_function1, \
    eve_custom_loss_function1, generate_model_alice, generate_model_eve, eve_custom_loss_function2, \
    bob_custom_loss_function2
import os

from utils.tensorboard_utils_functions import start_tensorboard_server

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

args = arg_parser.parse_arguments()

message_length = args.message_length
key_length = args.key_length
epochs = args.epochs
learning_rate = args.learning_rate
batch_size = args.batch_size
tensorboard_start_new_thread = args.tensorboard_start_new_thread


def main():

    alice = P.Personnage(
        prenom="Alice",
        model=generate_model_alice(key_length=key_length, message_length=message_length),
        loss_function=alice_custom_loss_function,
        cle=generate_boolean_list(batch_size, key_length),
    )

    alice.set_message(generate_boolean_list(batch_size, message_length))

    bob = P.Personnage(
        prenom="Bob",
        model=generate_model_alice(key_length=key_length, message_length=message_length),
        cle=copy.copy(alice.cle),
        loss_function=bob_custom_loss_function1
    )

    eve = P.Personnage(
        prenom="Eve",
        model=generate_model_eve(message_length=message_length),
        cle=None,
        loss_function=eve_custom_loss_function1
    )

    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)

    # Message initial en entrée d'Alice
    expected = tf.stack(generate_boolean_list(1, message_length))

    summary_writer = tf.summary.create_file_writer('logs/train_' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))

    start_tensorboard_server("logs", tensorboard_start_new_thread)

    for epoch in range(epochs):
        with tf.GradientTape(persistent=True) as tape:

            alice.calculate_message_out()

            bob.set_message(alice.message_out)
            bob.calculate_message_out()

            eve.set_message(alice.message_out)
            eve.calculate_message_out()

            bob.calculate_loss(y_pred=bob.message_out, y_true=expected)
            eve.calculate_loss(y_pred=eve.message_out, y_true=expected)
            alice.calculate_loss(message_length=message_length, bob_loss=bob.loss, eve_loss=eve.loss)

            with summary_writer.as_default():
                tf.summary.scalar('bob_loss', bob.get_loss_value(), step=epoch)
                tf.summary.scalar('eve_loss', eve.get_loss_value(), step=epoch)
                tf.summary.scalar('alice_loss', alice.get_loss_value(), step=epoch)

        grads_alice = tape.gradient(target=alice.loss, sources=alice.model.trainable_variables)
        grads_bob = tape.gradient(target=bob.loss, sources=bob.model.trainable_variables)
        grads_eve = tape.gradient(target=eve.loss, sources=eve.model.trainable_variables)

        optimizer.apply_gradients(
            zip(
                grads_alice,
                alice.model.trainable_variables,
            )
        )

        optimizer.apply_gradients(
            zip(
                grads_bob,
                bob.model.trainable_variables,
            )
        )

        optimizer.apply_gradients(
            zip(
                grads_eve,
                eve.model.trainable_variables,
            )
        )

    bob.print_report()
    eve.print_report()
    alice.print_report()


if __name__ == "__main__":
    main()
