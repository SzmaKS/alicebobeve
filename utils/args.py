from argparse import ArgumentParser


def parse_arguments():

    parser = ArgumentParser(
        description="Parse main script's arguments"
    )

    parser.add_argument(
        '--key-length',
        dest="key_length",
        type=int,
        help='Length of crypto key',
        required=False
    )

    parser.add_argument(
        '--message-length',
        dest="message_length",
        type=int,
        help='Length of the message',
        required=False
    )

    parser.add_argument(
        '--epochs',
        dest="epochs",
        type=int,
        help='Number of epochs',
        required=False
    )

    parser.add_argument(
        '--learning-rate',
        dest="learning_rate",
        type=float,
        help='Valeur de la learning rate pour la fonction d\'entraînement',
        required=False
    )

    parser.add_argument(
        '--iter-per-epoch',
        dest="iter_per_epoch",
        type=int,
        help='Nombre d\'itérations par epoch',
        required=False
    )

    parser.add_argument(
        '--batch-size',
        dest="batch_size",
        type=int,
        help='Taille du batch de données à utiliser',
        required=False
    )

    parser.add_argument(
        '--tensorboard-start-new-thread',
        dest="tensorboard_start_new_thread",
        type=bool,
        help='Détermine si un nouveau processus doit être créé pour Tensorboard',
        required=False
    )

    parser.set_defaults(
        key_length=60,
        message_length=60,
        epochs=5000,
        learning_rate=0.0008,
        iter_per_epoch=200,
        batch_size=60,
        tensorboard_start_new_thead=True
    )

    return parser.parse_args()
