import numpy as np
import tensorflow as tf
from tensorflow_core.python.keras.layers.core import Reshape, Flatten


def generate_boolean_list(size_x, size_y):
    temp = np.random.randint(2, size=[size_x, size_y])
    return temp.astype(np.float32)


def generate_model_alice(key_length, message_length):
    """
    Fonction créant le modèle propre à Alice et Bob
    :param key_length: Clé concaténée à la fin du message
    :param message_length: longueur du message d'entrée
    :return: Modèle de réseau de neuronnes
    """
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(message_length + key_length, input_shape=(key_length + message_length,), dtype='float32'))

    model.add(Reshape((message_length + key_length, 1)))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.sigmoid, padding='SAME'))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.sigmoid, padding='VALID'))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.sigmoid, padding='SAME'))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.tanh, padding='SAME'))
    model.add(Flatten())
    model.add(tf.keras.layers.Dense(units=message_length, dtype='float32'))
    return model


def generate_model_eve(message_length):
    """
    Fonction créant le modèle propre à Eve
    :param message_length: longueur du message d'entrée. Eve ne possède pas la clé
    :return: Modèle de réseau de neuronnes
    """
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(message_length, input_shape=(message_length,), dtype='float32'))

    model.add(Reshape((message_length, 1)))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.sigmoid, padding='SAME'))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.sigmoid, padding='VALID'))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.sigmoid, padding='SAME'))
    model.add(tf.keras.layers.Conv1D(1, 2, activation=tf.nn.tanh, padding='SAME'))
    model.add(Flatten())
    model.add(tf.keras.layers.Dense(units=message_length, dtype='float32'))
    return model


def alice_custom_loss_function(message_length, bob_loss, eve_loss):
    bob_loss = tf.cast(bob_loss, 'float32')
    eve_loss = tf.cast(eve_loss, 'float32')
    return bob_loss + 2*(1 - eve_loss)


def eve_custom_loss_function1(y_pred, y_true):
    sub = tf.abs(tf.subtract(tf.cast(y_pred, 'float32'), tf.cast(y_true, 'float32')))
    return tf.divide(tf.reduce_sum(sub), tf.cast(tf.size(y_pred), 'float32'))


def bob_custom_loss_function1(y_pred, y_true):
    sub = tf.abs(tf.subtract(tf.cast(y_pred, 'float32'), tf.cast(y_true, 'float32')))
    return tf.divide(tf.reduce_sum(sub), tf.cast(tf.size(y_pred), 'float32'))


def eve_custom_loss_function2(y_pred, y_true):
    return tf.keras.losses.mean_absolute_error(y_true, y_pred)


def bob_custom_loss_function2(y_pred, y_true):
    return tf.keras.losses.mean_absolute_error(y_true, y_pred)
