import multiprocessing
import os


def start_tensorboard_server(log_dir, new_thread=False):
    forbidden_chars = [";", ",", "&", "|"]

    for char in forbidden_chars:
        log_dir = log_dir.replace(char, "")

    p = multiprocessing.Process(target=os.system, args=("tensorboard --logdir " + log_dir,))

    if new_thread:
        p.daemon = True

    return p.start()
